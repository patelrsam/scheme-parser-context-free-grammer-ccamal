(* CMSC 330 Project - Scheme Parser & Interpreter *)
 
(* NAME: Shubham Patel*)
 
(* To load this file into the OCaml interpreter, type
 
   #use "scheme.ml"
 
   at the OCaml prompt
*)
 
#load "str.cma"

(* Use this as your abstract syntax tree *)
type ast =
  | Id of string
  | Num of int
  | Bool of bool
  | String of string
  | List of ast list

(* An unparser turns an AST back into a string.  You may find this
   unparser handy in writing this project *)
let rec unparse_list = function
  | [] -> "empty"
  | (x::[]) -> unparse x
  | (x::xs) -> (unparse x) ^ " " ^ (unparse_list xs)

and unparse = function
  | Id id -> id
  | Num n -> string_of_int n
  | Bool true -> "#t"
  | Bool false -> "#f"
  | String s -> "\"" ^ s ^ "\""
  | List l -> "(" ^ unparse_list l ^ ")"

(************************************************************************)
(* Scanner *)

type token =
 | Tok_Id of string
 | Tok_Num of int
 | Tok_String of string
 | Tok_True
 | Tok_False
 | Tok_LParen
 | Tok_RParen
 | Tok_END
 
(* 1 char tokens *)
let re_lparen = Str.regexp "("
let re_rparen = Str.regexp ")"
 
(* 2 char tokens *)
let re_true = Str.regexp "#t"
let re_false = Str.regexp "#f"
 
(* variable char tokens *)
let re_id = Str.regexp "[a-zA-Z=*+/<>!?-][a-zA-Z0-9=*+/<>!?-]*"
let re_num = Str.regexp "[-]*[0-9]+"
let re_string = Str.regexp "\"[^\"]*\""
let re_whitespace = Str.regexp "[ \t\n]"
 
exception Lex_error of int
 
let tokenize s =
 let rec tokenize' pos s =
   if pos >= String.length s then
     [Tok_END]
   else begin
     if (Str.string_match re_lparen s pos) then
       Tok_LParen::(tokenize' (pos+1) s)
     else if (Str.string_match re_rparen s pos) then
       Tok_RParen::(tokenize' (pos+1) s)
     else if (Str.string_match re_true s pos) then
       Tok_True::(tokenize' (pos+2) s)
     else if (Str.string_match re_false s pos) then
       Tok_False::(tokenize' (pos+2) s)
     else if (Str.string_match re_id s pos) then
       let token = Str.matched_string s in
       let new_pos = Str.match_end () in
       (Tok_Id token)::(tokenize' new_pos s)
     else if (Str.string_match re_string s pos) then
       let token = Str.matched_string s in
       let new_pos = Str.match_end () in
       let tok = Tok_String (String.sub token 1 ((String.length token)-2)) in
       tok::(tokenize' new_pos s)
     else if (Str.string_match re_num s pos) then
       let token = Str.matched_string s in
       let new_pos = Str.match_end () in
       (Tok_Num (int_of_string token))::(tokenize' new_pos s)
     else if (Str.string_match re_whitespace s pos) then
       tokenize' (Str.match_end ()) s
     else
       raise (Lex_error pos)
   end
 in
 tokenize' 0 s


(************************************************************************)
(*  
  S -> id | n | str | #t | #f | ( L )
  L -> S L | epsilon 

  First: 
  FIRST(S) = [id;n;str;#t;#f;(]
  FIRST(L) = [id;n;str;#t;#f;(;eps]
*)

let rec tkn_ls_to_str tkn = 
  match tkn with 
  [] -> ""
  |(Tok_Id s) :: t  -> s ^"; "^ (tkn_ls_to_str t)
  |(Tok_Num n) :: t -> (string_of_int n) ^": "^ (tkn_ls_to_str t)
  |(Tok_String s) :: t -> s ^"; "^  (tkn_ls_to_str t)
  |(Tok_True)::t -> "TRUE"^"; "^tkn_ls_to_str t
  |(Tok_False) :: t -> "FALSE" ^"; "^ tkn_ls_to_str t
  |(Tok_LParen) :: t -> "(" ^"; "^ tkn_ls_to_str t
  |(Tok_RParen) :: t -> ")"^"; "^ tkn_ls_to_str t
  |(Tok_END)::t -> ">>END<<"^tkn_ls_to_str t
;;

let prt_tkn_ls tkn = print_endline(tkn_ls_to_str tkn);;

let prt_tok tkn = 
  let rec hlp t = 
    match t with 
   | Tok_Id c -> c
   | Tok_Num n -> string_of_int n
   | Tok_String s-> s
   | Tok_True -> "TRUE"
   | Tok_False -> "FALSE"
   | Tok_LParen -> "("
   | Tok_RParen -> ")"
   | Tok_END -> "END"
in
print_endline( hlp tkn)
;;


exception YouDoneGoofedUp of string

let lookahead ls = 
  match ls with 
   [] -> raise (YouDoneGoofedUp "lookahead on empty list")
  |h::t -> (h,t)
;;

let rec parse_S (lst:token list) = (* ((Bool false), lst) *) (* TO DO *)
  let (tok,tail) = lookahead lst in
  match tok with 
    (Tok_Id s) -> (Id(s),tail)
    |(Tok_Num s) -> (Num(s),tail)
    |(Tok_String s) -> (String(s),tail)
    |(Tok_True) -> (Bool(true),tail)
    |(Tok_False) -> (Bool(false),tail)
    |(Tok_LParen) ->  let (a,tl) = parse_L tail in
                      let (tok2,tail2) = lookahead tl in
                      if (tok2 = Tok_RParen) then (a,tail2)
                      else raise (YouDoneGoofedUp "God Damn It Bobby - Check Your Elese in Tok_LParen")
    |_ -> raise (YouDoneGoofedUp "I’m Doped Out Of My Gourd - I Got No Idea What Ya Doin")


and parse_L lst = 
  let (tok,tail) = lookahead lst in
  match tok with 
  Tok_RParen -> (List [],lst)
  |Tok_END -> (List [],tail)
  |_->  let (a,tl) = parse_S lst in
        let (a2,tl2) = parse_L tl in 
          match a2 with 
          | List alst -> (List (a::alst),tl2)
          |_->raise (YouDoneGoofedUp "I’m Doped Out Of My Gourd - I Got No Idea What Ya Doin")
;;

type value =
    Val_Num of int
  | Val_Bool of bool
  | Val_String of string
  | Val_Null
  | Val_Cons of value * value
  | Val_Define of (string * value) list
  | Val_Closure (* of ... *) (* TO DO *)

(* The following function may come in handy *)
let rec string_of_value = function
    Val_Num n -> string_of_int n
  | Val_Bool true -> "#t"
  | Val_Bool false -> "#f"
  | Val_String s -> "\"" ^ s ^ "\""
  | Val_Null -> "null"
  | Val_Cons (v1, v2) -> "(cons " ^ (string_of_value v1) ^ " " ^
      (string_of_value v2) ^ ")"
  | Val_Define v -> "<define>"
  | Val_Closure -> "<closure>"

let parse lst =
  let (ast,lst2) = (parse_S lst) in ast
;;

(************************************************************************)
(* Write your evaluator here *)
exception SomthingIsFishy of string

let gt_vl id env = 
  if id = "null" then Val_Null
  else (List.assoc id env);;

let rec eval env ast = 
  match ast with 
    |Id i-> gt_vl i env
    |Num n -> Val_Num n
    |Bool b -> Val_Bool b 
    |String s -> Val_String s
    |List l ->  (* Ask About NULL And How TO Interpret It *)
                match l with 
                    Id("+")::ops -> pl_eval env ops
                  | Id("-")::ops -> mn_eval env ops
                  | Id("*")::ops -> ml_eval env ops
                  | Id("=")::ops -> eq_eval env ops 
                  | Id("if")::ops-> if_eval env ops
                  | Id("boolean?")::ops::[] -> bl_eval env ops
                  | Id("number?")::ops::[] -> nm_eval env ops
                  | Id("string?")::ops::[] -> st_eval env ops
                  | Id("pair?")::ops::[] -> pr_eval env ops
                  | Id("null?")::ops::[] -> nl_eval env ops
                  | Id("cons")::ops -> con_eval env ops
                  | Id("car")::ops::[]-> car_eval env ops
                  | Id("cdr")::ops::[]-> cdr_eval env ops
                  (* | Id("define")::i::val::[]-> define_eval env i val *)
                  | Id("define")::ops -> define_eval env ops
           
and pl_eval env op = 
  match op with 
    []->Val_Num(0)
    |h::t ->  let first = eval env h in 
              let second = pl_eval env t in 
              match first with 
                Val_Num fr -> 
                    match second with 
                      Val_Num(sd) -> Val_Num (fr+sd)
                      |_ -> raise (SomthingIsFishy "Good Luck With Your SD PLUS Eval")
                |_-> raise (SomthingIsFishy "Good Luck With Your FR PLUS Eval")

and mn_eval env op = 
  match op with 
    []->Val_Num(0)
    |h::t ->  let first = eval env h in 
              
              match first with 
                Val_Num fr -> 
                    (let second = pl_eval env t in 
                      match second with 
                        Val_Num(sd) -> Val_Num (fr-sd)
                        |_ -> raise (SomthingIsFishy "Good Luck With Your SD MINUS Eval"))
                |_-> raise (SomthingIsFishy "Good Luck With Your FR MINUS Eval")

and ml_eval env op = 
  match op with 
    []->Val_Num(1)
    |h::t ->  let first = eval env h in 
              let second = ml_eval env t in 
              match first with 
                Val_Num fr -> 
                    match second with 
                      Val_Num(sd) -> Val_Num (fr*sd)
                      |_ -> raise (SomthingIsFishy "Good Luck With Your SD MULTIPLY Eval")
                |_-> raise (SomthingIsFishy "Good Luck With Your FR MULTIPLY Eval")

and eq_eval env op = 
  match op with 
      [] -> Val_Bool(true)
    (* | h  -> Val_Bool(true) *)
    | h::t::[] -> let first = eval env h in 
              let second = eval env t in
              string_of_value second;
              match first with 
              Val_Num(fr) -> 
                            match second with
                              Val_Num(sd) -> if (fr = sd) then Val_Bool(true)
                                             else Val_Bool(false)
                              |_-> raise(SomthingIsFishy "What Are Ya Doing Comparing Num to Non-Num")
              |_->raise(SomthingIsFishy "What Are Ya Doing Comparing Non-Num to Num") 

(* and  *)
and if_eval env op = 
  match op with
    |cnd::tr::fl::[] -> let bl = eval env cnd in
                    (match bl with 
                      |Val_Bool(tf) -> if tf then eval env tr 
                                       else eval env fl
                      |_ -> raise (SomthingIsFishy "Dat If Cond Ain't Looking Right"))
    |cnd::tr::[] -> let bl = eval env cnd in 
                    match bl with 
                      |Val_Bool(tf) ->  if tf then eval env tr
                                        else Val_Null
                      |_ -> raise (SomthingIsFishy "Dat If Cond Ain't Looking Right")


and bl_eval env op = 
  let bl = eval env op in
  match bl with 
     Val_Bool(_) -> Val_Bool (true)
    |_ -> Val_Bool(false)

and nm_eval env op = 
  let nm = eval env op in 
  match nm with 
     Val_Num(_)-> Val_Bool(true)
    |_-> Val_Bool(false)

and st_eval env op = 
  let st = eval env op in
  match st with 
     Val_String(_)-> Val_Bool(true)
    |_-> Val_Bool(false)

and pr_eval env op = 
  let pr = eval env op in 
  match pr with 
     Val_Cons(_,_)-> Val_Bool(true)
    |_->Val_Bool(false)

and nl_eval env op = 
  let nl = eval env op in 
  match nl with 
     Val_Null -> Val_Bool (true)
    |_->Val_Bool(false)

and con_eval env op = 
  match op with 
    h::t::[] -> let f = eval env h in
                let l = eval env t in
            Val_Cons(f,l)
    |_ -> raise (SomthingIsFishy "Da Cons Givin Ya Trouble")

and car_eval env op = 
  let cr = eval env op in
  match cr with 
   Val_Cons(a,b) -> a
  |_-> raise (SomthingIsFishy "Da Car Givin Ya Trouble")

and cdr_eval env op =
  let cd = eval env op in
  match cd with 
     Val_Cons(a,b) -> b
    |_-> raise (SomthingIsFishy "Da Crd Givin Ya Trouble")


and define_eval env op =
  match op with 
     (Id h)::t::[] -> let vl = eval env t in
                  Val_Define((h,vl)::env)
    |_->raise (SomthingIsFishy "Da Define Givin Ya Trouble")







